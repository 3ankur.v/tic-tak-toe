import React, { useState, useEffect } from 'react';
// import logo from './logo.svg';
import './App.css';

function App() {

  const [xDraw, setXDraw] = useState(true);
  const [userAction, setUserAction] = useState({});
  const [winStatus, setWinStatus] = useState(null);
  const [corretPattern,setCorrectPatterm] = useState([]);
  const [newGame, setNewGame] = useState(false);
  const arr = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8]
  ];



  const startFreshGame = () =>{
    setNewGame(true)
    setUserAction({});
    setWinStatus(null);
    setCorrectPatterm([]);
  }

  const calculateWinner = (userData) => {
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ];
   // console.log(userData);

    let neverBreak = true;
    // keep it simple 
    for (let p of lines) {
      let tmpStr = '';
      for (let k of p) {
        if (userAction.hasOwnProperty(k)) {
          tmpStr = tmpStr + userAction[k];
          if (tmpStr === 'XXX') {
           // console.log("X Won")
            setWinStatus("X Has WON....!!")
            setCorrectPatterm(p);
            neverBreak = false;
            setTimeout(()=>{
              setNewGame(false);
            },1800);
          }
          else if (tmpStr === 'OOO') {
           // console.log("Y Won")
            setWinStatus("O Has WON....!!")
            setCorrectPatterm(p);
            setTimeout(()=>{
              setNewGame(false);
            },1800);
            neverBreak = false;
          }
        } else {
          neverBreak = false;
          break;
        }
      }
    }

    if (neverBreak) {
      setWinStatus("No One WON");
      setTimeout(()=>{
        setNewGame(false);
      },1800);
    }
  }




  useEffect(() => {
    calculateWinner()
   // console.log(corretPattern);
  }, [userAction])

  const drawIt = (block) => {
    if (!userAction[block]) {
      let tempAction = {};
      if (xDraw) {
       // console.log("X DID ")
        tempAction[block] = 'X';
      } else {
       // console.log("O DID")
        tempAction[block] = 'O';
      }
      setUserAction({
        ...userAction,
        ...tempAction
      })
      setXDraw(!xDraw);
     // console.log(userAction);
    }
  }

  return (
    <div className="App">
{
  newGame ? 

  <div className="main_game_container">
{
        winStatus && <h3>{winStatus}</h3>
      }
      <div className="game_container">
        {
          arr.map((item,idx) => {
            return (
              <div className="rows" key={`text${idx}`}>
                {
                  item.map((block,pix) => {
                    return (<div key={`block${pix}`} className={`block ${corretPattern.indexOf(block)>-1 ? 'matched' : ''}`}  onClick={() => drawIt(block)}> <span className="text-block"> {userAction[block]}</span></div>)
                  })
                }
              </div>
            )
          })
        }

      </div>
</div> :
<div className="start_game">
<button onClick={()=>startFreshGame()}  className="button" type="button">New Game</button>
</div>

}


    
    </div>
  );
}

export default App;
